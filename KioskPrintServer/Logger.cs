﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KioskPrintServer
{
    public static class Logger
    {
        public static void Write(string msg)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string subPath = "Logs";
            string logName = "KioskLog.txt";
            if (!Directory.Exists(path + @"\" + subPath)) Directory.CreateDirectory(path + @"\" + subPath);
            //System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\mazno\test.txt", true);
            System.IO.StreamWriter file = new System.IO.StreamWriter(path + @"\\" + subPath + @"\" + logName, true);
            file.WriteLine(DateTime.Now.ToString() + " : " + msg);

            file.Close();
        }


    }
}
