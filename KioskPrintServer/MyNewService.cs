﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using MySql;
using MySql.Data.MySqlClient;
using System.Threading;
using System.Printing;
using System.Drawing.Printing;
using System.Drawing;

namespace KioskPrintServer
{

    public partial class MyNewService : ServiceBase
    {
        public MyNewService()
        {
            this.ServiceName = "KioskPrintService";
            this.CanStop = true;
            this.CanPauseAndContinue = true;

            //Setup logging
            this.AutoLog = false;

            ((ISupportInitialize)this.EventLog).BeginInit();
            if (!EventLog.SourceExists(this.ServiceName))
            {
                EventLog.CreateEventSource(this.ServiceName, "Application");
            }
            ((ISupportInitialize)this.EventLog).EndInit();

            this.EventLog.Source = this.ServiceName;
            this.EventLog.Log = "Application";

            InitializeComponent();
        }


        protected override void OnStart(string[] args)
        {
            this.EventLog.WriteEntry("In OnStart");
            Thread t = new Thread(new ThreadStart(Program.KioskPrinter));
            t.Start();  
            
        }

        
        protected override void OnStop()
        {

        }

        

    }
}
