﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MySql;
using MySql.Data.MySqlClient;
using System.Threading;
using System.Printing;
using System.Drawing.Printing;
using System.Drawing;
using System.Diagnostics;
using System.Management;

namespace KioskPrintServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new MyNewService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        public static void KioskPrinter()
        {
            string printerLoc = System.Configuration.ConfigurationManager.AppSettings["ipadress"];
            string printerName = @"Posiflex PP9000 Printer";

            Logger.Write("Connecting to " + printerLoc);
            PrintServer printServer = new PrintServer(printerName);
            Logger.Write("Connected to " + printServer.Name);
            PrintQueueCollection printQueues = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
            PrintQueue printQueue = null;

            foreach (PrintQueue pq in printQueues)
            {
                if (pq.FullName == printerName)
                    printQueue = pq;
            }

            string server = System.Configuration.ConfigurationManager.AppSettings["server"];
            string database = System.Configuration.ConfigurationManager.AppSettings["database"];
            string userId = System.Configuration.ConfigurationManager.AppSettings["userId"];
            string password = System.Configuration.ConfigurationManager.AppSettings["password"];
            Logger.Write("Connecting to DB: " + server + "/" + database + "with user: " + userId);

            MySqlConnection connection = new MySqlConnection(String.Format("server={0};database={1};uid={2};pwd={3};", server, database, userId, password));
            try
            {
                Logger.Write("Connecting to DB");
                try
                {
                    connection.Open();
                }
                catch (MySqlException ex)
                {
                    Logger.Write("could not connect to mysql, retrying ");
                }

                while (true)
                {
                    if (connection.State.ToString() != "Open")
                    {
                        try
                        {
                            connection.Open();
                        }
                        catch (MySqlException ex)
                        {
                            Logger.Write("could not connect to mysql, retrying ");
                            continue;
                        }
                    }

                    printQueue.Refresh();
                    if (printQueue.NumberOfJobs == 0 && checkPrinterStatus())
                    {
                        using (MySqlCommand cmd = new MySqlCommand("select * from Tickets where status = 'waiting' limit 1", connection))
                        {
                            try
                            {
                                using (MySqlDataReader reader = cmd.ExecuteReader())
                                {
                                    if (!reader.HasRows)
                                    {
                                        Thread.Sleep(1000);
                                    }
                                    else
                                    {
                                        string id = "";
                                        string LotteryName = "";
                                        string Date = "";
                                        string TicketId = "";
                                        string PlayerName = "";

                                        while (reader.Read())
                                        {
                                            id = reader["id"].ToString();
                                            LotteryName = reader["LotteryName"].ToString();
                                            Date = reader["Date"].ToString();
                                            TicketId = reader["TicketId"].ToString();
                                            PlayerName = reader["PlayerName"].ToString();
                                        }
                                        //TODO send data to print and return result
                                        reader.Close();
                                        try
                                        {
                                            bool result = Print(printQueue, LotteryName, Date, TicketId, PlayerName);
                                            if (result)
                                            {
                                                string returnText = String.Format("update Tickets set status = 'done' where id = {0}", id);
                                                MySqlCommand returnCmd = new MySqlCommand(returnText, connection);
                                                try
                                                {
                                                    returnCmd.ExecuteNonQuery();
                                                }
                                                catch(MySqlException e)
                                                {
                                                    Logger.Write("could not execute query");
                                                }
                                               
                                            }


                                        }
                                        catch (Exception e)
                                        {
                                            Logger.Write(e.ToString());
                                            continue;
                                        }
                                    }

                                }
                            }
                            catch(MySqlException e)
                            {
                                Logger.Write("could not fetch info");
                            }
                            
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Write(e.ToString());
            }

            Console.ReadLine();
        }

        public static bool Print(PrintQueue pq, string lotteryName, string date, string ticketId, string playerName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(lotteryName + "\n" + date + "\n" + ticketId + "\n" + playerName);
            PrintDocument printDoc = new PrintDocument();
            printDoc.PrintPage += delegate(object sender, PrintPageEventArgs args)
            {
                args.Graphics.DrawString(sb.ToString(), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new RectangleF(0, 0, printDoc.DefaultPageSettings.PrintableArea.Width, printDoc.DefaultPageSettings.PrintableArea.Height));
            };
            
            printDoc.Print();
            
            pq.Refresh();
            PrintJobInfoCollection pColl = pq.GetPrintJobInfoCollection();
            DateTime currentDate = DateTime.Now;
            DateTime curDate = currentDate.AddSeconds(2);
            foreach (PrintSystemJobInfo job in pColl)
            {
                if (job != null)
                {
                    Logger.Write("Started working on Job : " + job.JobName);
                    var done = false;

                    while (!done && currentDate < curDate)
                    {
                        Logger.Write("logging");
                        currentDate = DateTime.Now;
                        pq.Refresh();
                        job.Refresh();
                        done = job.IsCompleted || job.IsDeleted || job.IsPrinted;

                        if (done)
                        {
                            Logger.Write("Finished working on Job : " + job.Name);
                            return true;
                        }
                    }
                    return false;

                }
                return false;
            }
            return false;
        }

        static bool checkPrinterStatus()
        {
            string printerName = @"Posiflex PP9000 Printer";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(String.Format("SELECT * FROM Win32_Printer WHERE Name LIKE '%{0}'", printerName));
            bool result = false;


            try
            {
                foreach (ManagementObject printer in searcher.Get())
                {
                    if (printer["WorkOffline"].ToString() == "True")
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            catch (ManagementException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;


        }
    }
}
