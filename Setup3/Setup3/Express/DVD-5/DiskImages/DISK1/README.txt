INSTALLATION:
1.Open Setup.exe and install the service
2.Open Local Services and locate KioskService
3.Go to properties -> Log On and use the machine's credentials to login
4.Go to installation folder (default: c:\\Program Files (x86)\Atom\KioskService)
5.Open 'KioskPrintServer.exe.config' and edit the important settings
6.Reset service from Local Services
7.Track logs in installation folder\Logs\